﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace TextAdventure
{
    public class Game
    {
        private String gameName = "The Dark Forest";   // a String initialized to empty
        private bool shouldExit = false; // Create a bool (true/false logical operator) indicating if we should exit the game

        // Constructor for the Game class
        // This is the code that is called whenever an instance of Game is created
        public Game()
        {
            
        }

        // Function (segment of logic) called from Program.cs when the application starts

        public void Begin()
        {
            // Print various lines of strings to the Console window
            Console.WriteLine("************************************");
            Console.WriteLine("Welcome to " + gameName);    // the + operator concatenate two strings (combines them)

            Console.WriteLine("You are in a dark, strange forest in the middle of the night. You do not know why you are here, or where you came from, but you know that there is a mission at hand: escape. What to do: ");
            Console.WriteLine("1 eat, 2 run, 3 open eyes.");

            // Loop until shouldExit is true
            while (shouldExit == false)
            {
                String command = Console.ReadLine();    // Read a line from Console (when Enter is hit) and store it in command

                HandleCommand(command);
            }
        }

        public void HandleCommand(String command)
        {
            // Enforce that the String is lowercase
            command = command.ToLower();

            if (command == "3")
            {
                Console.WriteLine("That's a pretty good idea. You see some trees, a baseball bat, and darkness. What to do next: ");
                Console.WriteLine("1 pick up bat, 2 run, 3 look for people");

                String aResponse = Console.ReadLine();

                if (aResponse == "1")
                {
                    Console.WriteLine("It might not be a gun, but it still hurts. What to do next: ");
                    Console.WriteLine("1 look for people, 2 find food, 3 find water");

                    String bResponse = Console.ReadLine();

                    if (bResponse == "1")
                    {
                        Console.WriteLine("As you walk along for a few hours, you thirst for water. Set upon one goal in mind, you wander aimlessly looking for human life, and eventually dehydrate. You lost.");
                    }
                    else if (bResponse == "2")
                    {
                        Console.WriteLine("You obviously never watched a survival show before. People dehydrate before they go hungry, therefor, you lost.");
                    }
                    else if (bResponse == "3")
                    {
                        Console.WriteLine("As any intelligent person does, you search for water before food. As the sun begins to rise, you find a small river, determine that it is clean, and as a bonus, found an apple tree! As you eat, you see a storm fast approaching. What to do next: ");
                        Console.WriteLine("1 run away from the storm, 2 sleep under a tree with plenty of leaves, 3 sleep in a nearby cave");

                        String cResponse = Console.ReadLine();

                        if (cResponse == "1")
                        {
                            Console.WriteLine("That was a pretty dumb answer. Storms always win races. You lost. Miserably.");
                        }
                        else if (cResponse == "3")
                        {
                            Console.WriteLine("You run to a nearby cave. as you lay down, you realize you are sharing a cave with an angry momma bear protecting her cubs. You lost.");
                        }
                        else if (cResponse == "2")
                        {
                            Console.WriteLine("You suffer through the storm and the cold, but survive. When you wake up, the sun just begins to shine through the clouds. You turn your head, only to see a rugged looking mountain man running at you with a shotgun in hand. What to do: ");
                            Console.WriteLine("1 politely ask what's wrong, 2 punch him, 3 hit him with the bat");

                            String dResponse = Console.ReadLine();

                            if (dResponse == "2")
                            {
                                Console.WriteLine("As you deliver a powerful blow, the mountainman shakes it off and shows his superior melee skills, knocking you out in three quick hits. You lost.");
                            }
                            else if (dResponse == "3")
                            {
                                Console.WriteLine("Since we all know violence doesn't pay off, the mountainman grabs the bat as you swing, rips it from your hands, and knocks you unconcious. You lost.");
                            }
                            else if (dResponse == "1")
                            {
                                Console.WriteLine("You work up all your courage and stand your ground, politely asking 'What's up?' He grabs you by the shoulders, and as he speaks he spits in your face, 'You have to help me! I was about to kill the last sasquatch in the world, when it suddenly dissappeared! You gotta help me!' WHat to do: ");
                                Console.WriteLine("1 refuse his request, 2 agree to help and ask questions, 3 be a tough guy and without a word grab his shotgun and run off into the woods");

                                String eResponse = Console.ReadLine();

                                if (eResponse == "1")
                                {
                                    Console.WriteLine("You refuse the man's request, find your way back to civilization, and return to your repetitive, boring life. Technically, you won because you are still alive and healthy. Then again, you opted for the boring choice, returned back to your monotonous life, and did nothing of importance of your life. Try to go farther next time.");
                                }
                                else if (eResponse == "3")
                                {
                                    Console.WriteLine("As always, your tough guy attitude doesn't pay off. You ander aimlessly, knowing nothing of the forest and you get lost. You lost.");
                                }
                                else if (eResponse == "2")
                                {
                                    Console.WriteLine("You ask him some questions, such as his name, where he first saw the sasquatch, and where he lost sight of the sasquatch. with this information; what to do next: ");
                                    Console.WriteLine("1 adopt a 'only idiots follow directions, I'm going to do this all on my own' attitude, 2 go off and find the sasquatch, 3 use the power of a lackluster background story of your character to summon survival and hunting skills");

                                    String fResponse = Console.ReadLine();

                                    if (fResponse == "1")
                                    {
                                        Console.WriteLine("Due to your want to NOT follow directions, you get lost and live the rest of your life in the forest in solitude. You lost.");
                                    }
                                    else if (fResponse == "2")
                                    {
                                        Console.WriteLine("Because you do not actually know anything about hunting or survival, you arrive at the place the mountainman last saw the sasquatch and sit there, dumbfounded. You lost.");
                                    }
                                    else if (fResponse == "3")
                                    {
                                        Console.WriteLine("You use your newly found vivid memories of your time in the boyscouts to look for footprints, trails, and sasquatch droppings. Also, as any good survival expert, you take the mountainman's shotgun. Suddenly, you spot a big, scary, nasty, hairy, and smelly sasquatch a few feet in the distance. He does not sense your presence. What to do next: ");
                                        Console.WriteLine("1 like any good videogame player, pull off a 'no scope headshot', firing from the hip, 2 appreciate the beauty of the sasquatch and walk the other way, 3 take aim, steady your breathing, and slowly pull the trigger");

                                        String gResponse = Console.ReadLine();

                                        if (gResponse == "3")
                                        {
                                            Console.WriteLine("You pull the trigger, and as the sasquatch yells in pain, you run closer. 'You cruel man/woman! I am the last of my kind, and you kill me! We were peaceful, and we protected your kind since the beginnin of time! How could you do this to us!' he yells. You are speechless. As he dies, you begin to feel a great sense of regret. On one hand, you won in that the mountainman brought you back to town and you became ridiculously rich and famous. On the other hand, you single-handedly caused the extinction of a majestic, mysterious, and peaceful species. So... you also kind of lost. Good job.");
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("I don't understand.");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("I don't understand.");
                                }
                            }
                            else
                            {
                                Console.WriteLine("I don't understand.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("I don't understand.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("I don't understand.");
                    }
                }
                else if (aResponse == "2")
                {
                    Console.WriteLine("You begin to run... and run... and run... and eventually dehydrate. You lost.");
                }
                else if (aResponse == "3")
                {
                    Console.WriteLine("With one goal in mind, you search endlessly, only to eventually dehydrate. You lost.");
                }
                else
                {
                    Console.WriteLine("I don't understand.");
                }
            }
            else if (command == "1")
            {
                Console.WriteLine("You run into a tree and go unconcious. You lost.");
            }
            else if (command == "2")
            {
                Console.WriteLine("You run into a tree and go unconcious. You lost.");
            }
            else
            {
                Console.WriteLine("I don't understand.");
            }
        }
    }
}
