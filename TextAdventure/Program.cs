﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextAdventure
{
    class Program
    {
        private static Game game;

        static void Main(string[] args)
        {
            // Create new instance of the Game class
            game = new Game();

            // Call various functions
            game.Begin();
        }
    }
}
